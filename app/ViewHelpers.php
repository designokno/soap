<?php

namespace App;

class ViewHelpers
{

    private $_js = [];
    private $_css = [];
    private $_jsCode = [];

    public function registerJs($src, $internal = true)
    {
        $resourcesDir = '';
        $path = ($internal ? $resourcesDir . '/' : '') . $src;
        if (!in_array($path, $this->_js)) {
            $this->_js[] = $path;
        }
        return $this;
    }

    public function registerCss($src, $internal = true)
    {
        $resourcesDir = '';
        $path = ($internal ? $resourcesDir . '/' : '') . $src;
        if (!in_array($path, $this->_css)) {
            $this->_css[] = $path;
        }
        return $this;
    }

    public function registerJsCode($code)
    {
        $this->_jsCode[] = $code;
        return $this;

    }

    public function renderJs()
    {

        $return = [];
        foreach ($this->_js as $src) {
            $return[] = "<script type='text/javascript' src='{$src}'></script>";
        }

        $returnCode = implode("\n", $this->_jsCode);
        $return[] = "<script type='text/javascript'>{$returnCode}</script>";

        return implode("\n", $return);

    }

    public function renderCss()
    {

        $return = [];
        foreach ($this->_css as $src) {
            $return[] = "<link rel='stylesheet' href='{$src}'>";
        }
        return implode("\n", $return);

    }
}