<?php

namespace App\Http\Controllers;

use App\Client1C;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    /**
     * @var \Illuminate\Redis\RedisManager
     */
    private $_redis = null;
    /**
     * @var Client1C
     */
    private $_soap = null;

    public function __construct()
    {
        parent::__construct();

        $this->_soap = $this->_app['soap'];
        $this->_redis = $this->_app['redis'];
    }

    public function indexAction(Request $request)
    {
        $user = $request->user();

        $result = $this->_soap->getreportlist(['login' => $user->login]);
        $reports = json_decode($result->return, true);


        $params = [
            'reports' => $reports,
        ];
        return $this->render('report/index', $params);
    }


    public function reportparamsAction(Request $request, $identifier)
    {
        $user = $request->user();
        $result = $this->_soap->getreportparam(
            [
                'reportid' => $identifier,
            ]
        );
        $params = json_decode($result->return, true);
        $params = array_map(function ($el) {
            $type = mb_strtolower($el['ТипПараметра']);
            $values = $type === 'list' ? explode("#", $el['ИмяВОтчете']) : null;
            return [
                'title' => $el['Имя'],
                'type' => $type,
                'values' => $values,
            ];
        }, $params);
        return response()->json([
            'params' => $params,
        ]);
    }


    public function reportgenerateAction(Request $request)
    {
        $user = $request->user();

        $identifier = $request->input('identifier');
        $params = $request->input('param');


        $paramJson = array_map(function ($k, $v) use ($identifier) {
            return [
                'name' => $k,
                'result' => $v,
                'idreport' => $identifier
            ];
        }, array_keys($params), $params);

        $paramJson[] = [
            'name' => 'login',
            'result' => $user->login,
            'idreport' => $identifier
        ];

        $result = $this->_soap->getreport(
            [
                'param' => json_encode($paramJson),
            ]
        );
        $report_link = $result->return;


        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $report_link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        if(empty($data)){
            return abort(404);
        }

        return $this->render('report/generate', [
            'report' => $data
        ]);
    }
}
