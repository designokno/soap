<?php

namespace App\Http\Controllers;

use App\Client1C;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    /**
     * @var \Illuminate\Redis\RedisManager
     */
    private $_redis = null;
    /**
     * @var Client1C
     */
    private $_soap = null;

    public function __construct()
    {
        parent::__construct();

        $this->_soap = $this->_app['soap'];
        $this->_redis = $this->_app['redis'];
    }

    public function indexAction(Request $request)
    {
        $user = $request->user();
        $text = $request->input('text');

        $table = [];
        if (!empty($text)) {
            $result = $this->_soap->searchzayav(['sessionid' => $user->session_id, 'str' => $text]);
            $table = json_decode($result->return, true);
        }

        $table_keys = [];
        if ($table && is_array($table)) {
            foreach ($table as $row) {
                $table_keys = array_merge(array_keys($row), $table_keys);
            }
        } else {
            $table = [];
        }
        $table_keys = array_unique($table_keys);
        return $this->render('search/index', [
            'table_keys' => $table_keys,
            'table_rows' => $table,
            'text'=>$text
        ]);
    }
}
