<?php

namespace App\Http\Controllers;

use App\SmsC;
use Illuminate\Http\Request;

class LoginController extends Controller
{

    /**
     * @var \Illuminate\Redis\RedisManager
     */
    private $_redis = null;

    /**
     * @var \SoapClient
     */
    private $_soap = null;

    private $_code_path = 'codes:';

    public function __construct()
    {
        parent::__construct();

        $this->_soap = $this->_app['soap'];
        $this->_redis = $this->_app['redis'];
    }

    public function indexAction(Request $request)
    {
        $params = [
            'message' => ''
        ];

        if ($request->isMethod('post')) {
            $login = $request->post('login');
            $password = md5($request->post('password'));
//            $session_id = $request->post('sessionid');

            $code_generator = $this->_app['code_generator'];
            $answer = $this->_app['soap']->loginuser([
                'name' => $login,
                'pass' => $password,
                'ip'=>$_SERVER['REMOTE_ADDR']
            ]);



            $data = json_decode($answer->return, true);
            $data = $data[0];

            $phone = intval($data['phone']);
            $email = $data['email'];
            $session_id = $data['sessionid'];
            $role = $data['account'];
            $fio = $data['fio'];

            if ($phone == 1) {
                $params['message'] = 'Номер телефона не задан';
            } elseif ($phone < 1) {
                $params['message'] = 'Неправильно введен логин или пароль';
            } else {
                if(env('SMS_LOGIN')) {
                    // generate sms code, than store it in redis and send. finally redirect to login/code

                    do {
                        $code = $code_generator->getCode();
                    } while ($this->_redis->hgetall($code));

                    /**
                     * @var $smsc SmsC
                     */
                    $smsc = $this->_app['smsc'];

                    $this->_redis->hmset("{$this->_code_path}{$code}", [
                        'login' => $login,
                        'password' => $password,
                        'session_id' => $session_id,
                        'phone'=>$phone,
                        'email'=>$email,
                        'role'=> $role,
                        'username'=>$fio
                    ]);

                    $smsc->send_sms($phone, $code);

                    return redirect()->route('login_code');
                }
                else{
                    $session = $request->session();
                    $session->put([
                        'login' => $login,
                        'password' => $password,
                        'session_id' => $session_id,
                        'phone'=>$phone,
                        'email'=>$email,
                        'role'=> $role,
                        'username'=>$fio
                    ]);
                    return redirect()->route('table_index');
                }
            }
        }

        return view('login/index', $params);
    }

    public function codeAction(Request $request)
    {
        $params = [
            'message'=>''
        ];

        $session = $request->session();

        if($request->isMethod('post')){
            $codeKey = "{$this->_code_path}{$request->input('code')}";
            $data = $this->_redis->hgetall($codeKey);
            if(empty($data)){
                $params['message'] = 'Вы ввели неверный код';
            }else{
                /**
                 * @var \Illuminate\Session\SessionManager
                 */

                $session->put($data);

                $this->_redis->del([$codeKey]);
                return redirect()->route('table_index');

            }
        }


        return view('login/code', $params);
    }

    public function logoutAction(Request $request)
    {

        $session = $request->session();

        $session->flush();


        return redirect()->to('/');
    }
}
