<?php

namespace App\Http\Controllers;

use App\Client1C;
use Illuminate\Http\Request;

class TableController extends Controller
{

    const ZAYAVKA_KEY = 'Заявка';
    const ZAMER_KEY = 'Замер';
    /**
     * @var \Illuminate\Redis\RedisManager
     */
    private $_redis = null;
    /**
     * @var Client1C
     */
    private $_soap = null;

    public function __construct()
    {
        parent::__construct();

        $this->_soap = $this->_app['soap'];
        $this->_redis = $this->_app['redis'];
    }

    public function indexAction(Request $request)
    {
        $user = $request->user();
        $date = $request->input('date');
        $parsed_date = date_parse_from_format("Y.m.d", $date);


        if (!$parsed_date['year']) {
            $date = date("Y.m.d");
        } else {
            $date = date("Y.m.d", mktime(0, 0, 0, $parsed_date['month'], $parsed_date['day'], $parsed_date['year']));
        }

        $template = "table/index";
        if ($user->hasRole('manager')) {
            $template = "table/index_manager";
            $result = $this->_soap->gettablemanager(['sessionid' => $user->session_id, 'dt' => $date]);
            $table = json_decode($result->return, true);
        } else {
            $template = "table/index";
            $result = $this->_soap->gettable(['sessionid' => $user->session_id, 'dt' => $date]);
            $table = json_decode($result->return, true);
        }


        $table_keys = [];
        if ($table && is_array($table)) {
            foreach ($table as $row) {
                $table_keys = array_merge(array_keys($row), $table_keys);
            }
        } else {
            $table = [];
        }
        $table_keys = array_unique($table_keys);

        $params = [
            'table_keys' => $table_keys,
            'table_rows' => $table,
            'date' => $date
        ];
        return $this->render($template, $params);
    }

    public function detailAction(Request $request, $zamerId)
    {
        $user = $request->user();

        $template = "table/detail";
        if ($user->hasRole('manager')) {
            $template = "table/detail_manager";
            $result = $this->_soap->getdetailedmanager(['sessionid' => $user->session_id, 'itemnum' => $zamerId]);
        } else {
            $template = "table/detail";
            $result = $this->_soap->getzamer(['sessionid' => $user->session_id, 'zamernum' => $zamerId]);
        }
        $detail = json_decode($result->return, true);
        $detail = $detail ? array_shift($detail) : [];
        $state = null;
        if (isset($detail['Состояние'])) {
            $state = $detail['Состояние'];
            unset($detail['Состояние']);
        }

        return $this->render($template, [
            'detail' => $detail,
            'state' => $state,
            'zayavNum' => $request->get('zayavNum'),
            'zamerId' => $zamerId,
            'date' => $request->get('date')
        ]);
    }

    public function filesAction(Request $request, $zayavNum)
    {
        $user = $request->user();
        $result = $this->_soap->getfilelist(['sessionid' => $user->session_id, 'zayavnum' => $zayavNum]);
        $files = json_decode($result->return, true);
        $fileTypeList = $this->_soap->getFileTypeList();

        if ($user->hasRole('manager')) {
            $result = $this->_soap->getdetailedmanager(['sessionid' => $user->session_id, 'itemnum' => $request->get('zamerId')]);
        } else {
            $result = $this->_soap->getzamer(['sessionid' => $user->session_id, 'zamernum' => $request->get('zamerId')]);
        }

        $detail = json_decode($result->return, true);
        $detail = $detail ? array_shift($detail) : [];
        $state = null;
        if (isset($detail['Состояние'])) {
            $state = $detail['Состояние'];
            unset($detail['Состояние']);
        }

        return $this->render('table/files', [
            'files' => is_array($files) ? $files : [],
            'fileTypeList' => $fileTypeList ? $fileTypeList : [],
            'zayavNum' => $zayavNum,
            'zamerId' => $request->get('zamerId'),
            'date' => $request->get('date'),
            'state' => $state,
        ]);
    }

    public function downloadAction(Request $request, $fileId)
    {
        $result = $this->_soap->getfile(['fileuid' => $fileId]);
        try {


            $filePath_a = explode('/', $result->return);
            $fileName = array_pop($filePath_a);
            list(, $ext) = explode('.', $fileName);

            $ch = curl_init();
            $timeout = 5;
            $url = str_replace(" ", "%20", $result->return);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            curl_close($ch);

            $tmpdir = env('TMP_DIR');
            $filePath = storage_path("{$tmpdir}/{$fileId}.{$ext}");
            file_put_contents($filePath, $data, FILE_BINARY);


            $file = new \SplFileInfo($filePath);
            $response = response()->download($file);
            $response->deleteFileAfterSend(true);
            return $response;
        } catch (\Exception $e) {
            return abort(404);
        }
    }

    public function uploadAction(Request $request, $zayavNum)
    {

        $this->validate($request, [
            'file' => 'required|max:5120',
        ]);

        $file = $request->file('file');
        $fileType = $request->input('filetype');
        $user = $request->user();

        $public_tmp_path = app()->basePath(env('TMP_DIR_PUBLIC'));
        $file->move($public_tmp_path, $file->getClientOriginalName());


        $result = $this->_soap->savefile(
            [
                'temppath' => "/" . env('TMP_DIR_PUBLIC_NAME'),
                'zamernum' => $request->get('zamerId'),
                'filetype' => $fileType,
                'filename' => $file->getClientOriginalName(),
                'login' => $user->login
            ]
        );

        return response()->json(['result' => 'ok']);
    }

    public function saveresultzamerAction(Request $request, $zamerId)
    {


        $this->validate($request, [
            'rezultzamer' => 'required',
            'status' => 'required',
        ]);

        $user = $request->user();
        $rezultzamer = $request->input('rezultzamer');
        $comment = $request->input('comment');
        $status = $request->input('status');

        $result = $this->_soap->saveresultzamer(
            [
                'sessionid' => $user->session_id,
                'zamernum' => $zamerId,
                'rezultzamer' => $rezultzamer,
                'comment' => $comment,
                'status' => $status,

            ]
        );

        return response()->json([
            'result' => $result->return == 1 ? true : false,
            'message' => $result->return
        ]);
    }

    public function savedogovorAction(Request $request, $zamerId)
    {
/*
        $this->validate($request, [
            'summadogovor' => 'required',
            'dtdostavka' => 'required',
            'dtmontage' => 'required',
            'summapredoplata' => 'required',
            'typeoplata' => 'required',
        ]);*/

        $user = $request->user();
        $summadogovor = $request->input('summadogovor');
        $dtdostavka = $request->input('dtdostavka');
        $dtmontage = $request->input('dtmontage');
        $isdemontage = boolval($request->input('isdemontage')) ? 1 : 0;
        $summapredoplata = $request->input('summapredoplata');
        $typeoplata = $request->input('typeoplata');
        $comment = $request->input('comment');
        $commentmontage = $request->input('commentmontage');
        $commentdostavka = $request->input('commentdostavka');

        $result = $this->_soap->savedogovor(
            [
                'sessionid' => $user->session_id,
                'zamernum' => $zamerId,
                'summadogovor' => $summadogovor,
                'dtdostavka' => $dtdostavka,
                'dtmontage' => $dtmontage,
                'isdemontage' => $isdemontage,
                'summapredoplata' => $summapredoplata,
                'typeoplata' => $typeoplata,
                'comment' => $comment,
                'commentmontage' => $commentmontage,
                'commentdostavka' => $commentdostavka,
            ]
        );


        return response()->json([
            'result' => $result->return == 1 ? true : false,
            'message' => $result->return
        ]);
    }

    public function setpayAction(Request $request, $zamerId)
    {
/*
        $this->validate($request, [
            'summ' => 'required',
            'dogovor' => 'required',
            'typeoplata' => 'required',
        ]);*/

        $user = $request->user();
        $summ = $request->input('summ');
        $dogovor = $request->input('dogovor');
        $typeoplata = $request->input('typeoplata');

        $result = $this->_soap->dogovor(
            [
                'login' => $user->login,
                'dogovor' => $dogovor,
                'typeoplata' => $typeoplata,
                'summ' => $summ,
            ]
        );

        return response()->json([
            'result' => $result->return == 1 ? true : false,
            'message' => $result->return
        ]);
    }


    public function additemAction(Request $request)
    {

//        $this->validate($request, [
//            'sourceitem' => 'required',
//            'name' => 'required',
//            'phone1' => 'required',
//            'address' => 'required',
//            'email' => 'required',
//            'inforeklama' => 'required',
////            'comment' => 'required',
//            'result' => 'required',
//        ]);

        $user = $request->user();
        $result = $this->_soap->additem(
            [
                'sessionid' => $user->session_id,
                'sourceitem' => $request->input('sourceitem'),
                'name' => $request->input('name'),
                'phone1' => $request->input('phone1'),
                'phone2' => $request->input('phone2'),
                'address' => $request->input('address'),
                'email' => $request->input('email'),
                'inforeklama' => $request->input('inforeklama'),
                'comment' => $request->input('comment'),
                'result' => $request->input('result'),
            ]
        );

        return response()->json([
            'result' => $result->return == 1 ? true : false,
            'message' => $result->return
        ]);
    }

    public function addzamerAction(Request $request, $itemnum)
    {

        /*$this->validate($request, [
            'dt' => 'required',
            'time' => 'required',
            'info' => 'required',
//            'comment' => 'required',
            'status' => 'required',
            'object' => 'required',
        ]);*/

        $user = $request->user();
        $result = $this->_soap->addzamer(
            [
                'sessionid' => $user->session_id,
                'itemnum' => $itemnum,
                'dt' => $request->input('dt'),
                'time' => $request->input('time'),
                'info' => $request->input('info'),
                'comment' => $request->input('comment'),
                'status' => $request->input('status'),
                'object' => $request->input('object'),
            ]
        );

        return response()->json([
            'result' => $result->return == 1 ? true : false,
            'message' => $result->return
        ]);
    }

    public function setstatusrouteAction(Request $request, $zamerId)
    {

        $this->validate($request, [
            'status' => 'required',
        ]);

        $user = $request->user();
        $status = $request->input('status');

        $result = $this->_soap->setstatusroute(
            [
                'login' => $user->login,
                'zamernum' => $zamerId,
                'status' => $status
            ]
        );

        return response()->json([
            'result' => $result->return == 1 ? true : false,
            'message' => $result->return
        ]);
    }


}
