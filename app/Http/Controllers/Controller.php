<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    protected $_layout = 'layout';

    /**
     * @var \Laravel\Lumen\Application|mixed|null
     */
    protected $_app = null;

    public function __construct()
    {
        $this->_app = app();
    }

    protected function render($view = null, $data = [], $mergeData = [])
    {
        return view($this->_layout, [
            'content' => view($view, $data, $mergeData)
        ]);
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    protected function logger()
    {
        return app()->log;
    }
}
