<?php

namespace App;

class Client1C extends \SoapClient
{


    public function getStatusZamerList()
    {
        $result = parent::getstatuszamerlist();
        $list = json_decode($result->return, true);
        $list = array_map(function ($el) {
            return $el['Наименование'];
        }, $list);
        return array_combine($list, $list);
    }

    public function getResultZamerList()
    {
        try {
            $result = parent::getrezultzamerlist();


            $list = json_decode($result->return, true);
            $list = array_map(function ($el) {
                return $el['Наименование'];
            }, $list);
            return array_combine($list, $list);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function getOpenDate($type)
    {
        $result = parent::getopendate([
            'typelimit' => $type
        ]);
        $list = json_decode($result->return, true);
        $list = array_map(function ($el) {
            return $el['Дата'];
        }, $list);
        return array_combine($list, $list);
    }


    public function getOplataTypeList()
    {
        try {
            $result = parent::getoplatatypelist();

            $list = json_decode($result->return, true);
            $list = array_map(function ($el) {
                return $el['Наименование'];
            }, $list);
            return array_combine($list, $list);
        } catch (\Exception $e) {
            return [];
        }
    }


    public function getFileTypeList()
    {
        $result = parent::getfiletypelist();
        $fileTypeList = json_decode($result->return, true);

        $fileTypeList = array_map(function ($el) {
            return $el['ВидФайла'];
        }, $fileTypeList);
        return array_combine($fileTypeList, $fileTypeList);
    }

    public function getDogovorList($zayavNum)
    {
        try {
            $result = parent::getdogovorlist(['zayav' => $zayavNum]);
            $dogovorList = json_decode($result->return, true);


            $dogovorList = array_map(function ($el) {
                return $el['НомерДоговора'];
            }, $dogovorList);
            return array_combine($dogovorList, $dogovorList);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function getVariantOplataList()
    {
        try {
            $result = parent::getlistvariantoplata();
            $paymentTypesList = json_decode($result->return, true);

            $paymentTypesList = array_map(function ($el) {
                return $el['Наименование'];
            }, $paymentTypesList);
            return array_combine($paymentTypesList, $paymentTypesList);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function renderFormField($title, $funcname, $fieldname, $params = [])
    {
//        try {
        $user = app()->request->user();

        $result = parent::getdefault([
            'sessionid' => $user->session_id,
            'func' => $funcname,
            'name' => $fieldname
        ]);


        $fieldData = json_decode($result->return, true);

        $default_template = 'components/form/fields/input_text';
        $items = [];
        if (is_array($fieldData)) {
            $default_template = 'components/form/fields/select';
            $items = array_map(function ($el) {
                return $el['Наименование'];
            }, $fieldData);
        }


        $template = isset($params['template']) ? $params['template'] : $default_template;


        return view($template, [
            'title' => $title,
            'funcname' => $funcname,
            'fieldname' => $fieldname,
            'required' => isset($params['required']) ? $params['required'] : false,
            'items' => $items
        ]);
//        } catch (\Exception $e) {
//            return '';
//        }
    }
}