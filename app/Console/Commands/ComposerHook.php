<?php
namespace App\Console\Commands;


use Illuminate\Console\Command;

class ComposerHook extends Command
{

    protected $signature = 'composer:hook {action}';
    protected $description = 'Scripts for composer hooks';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument('action') . "Hook";
        return $this->$action();
    }


    public function postInstallHook(){
        $tmp_path = storage_path(env('TMP_DIR'));

        if (!file_exists($tmp_path)) {
            mkdir($tmp_path);
            $this->info("tmp folder was created");
        }else{
            $this->info("tmp folder exists");
        }

        $public_tmp_path = app()->basePath(env('TMP_DIR_PUBLIC'));
        if (!file_exists($public_tmp_path)) {
            mkdir($public_tmp_path);
            $this->info("public tmp folder was created");
        }else{
            $this->info("public tmp folder exists");
        }
    }
}

