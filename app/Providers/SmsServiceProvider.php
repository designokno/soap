<?php

namespace App\Providers;

use App\CodeGenerator;
use App\SmsC;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('smsc', function ($app) {
            return new SmsC(env('SMSC_LOGIN'), env('SMSC_PASSWORD'));
        });
        $this->app->singleton("code_generator", function ($app) {
            return new CodeGenerator();
        });
    }
}

