<?php

namespace App\Providers;

use App\Client1C;
use App\CodeGenerator;
use App\SmsC;
use Illuminate\Support\ServiceProvider;

class SoapServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('soap', function ($app) {
            $host = env('CRM_HOST');
            $http_login = env('CRM_LOGIN');
            $http_password = env('CRM_PASSWORD');
            $crm_env = env('CRM_ENV', 'CRM_TEST');

            $soap_params = null;
            if ($http_login && $http_password){
                $soap_params = [
                    'login'=>$http_login,
                    'password'=>$http_password,
                ];
            }
            $client = new Client1C("{$host}/{$crm_env}/WebService.1cws?wsdl",$soap_params);
            return $client;
        });
    }
}

