<?php

namespace App\Providers;

use App\Client1C;
use App\CodeGenerator;
use App\SmsC;
use App\ViewHelpers;
use Illuminate\Support\ServiceProvider;

class ViewHelpersProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('viewHelpers', function ($app) {
            $viewHelpers = new ViewHelpers();
            return $viewHelpers;
        });
    }
}

