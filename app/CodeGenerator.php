<?php

namespace App;

use Illuminate\Support\Str;

class CodeGenerator
{

    public function getCode(){
        return mt_rand(1000000, 9999999);
    }

}