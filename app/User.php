<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

//    /**
//     * The attributes that are mass assignable.
//     *
//     * @var array
//     */
//    protected $fillable = [
//        'name', 'email',
//    ];
//
//    /**
//     * The attributes excluded from the model's JSON form.
//     *
//     * @var array
//     */
//    protected $hidden = [
//        'password',
//    ];

    public $password;
    public $login;

    public $session_id;
    public $phone;
    public $role;
    public $username;

    public function __construct($login, $password, $session_id, $data)
    {
        $this->login = $login;
        $this->password = $password;
        $this->session_id = $session_id;

        $this->phone = isset($data['phone'])?$data['phone']:null;
        $this->role = isset($data['role'])?$data['role']:null;
        $this->username = isset($data['username'])?$data['username']:null;
    }

    public function getKeyName(){
        return 'login';
    }


    public function hasRole($role){
        /**
         * @var $config \Illuminate\Config\Repository
         */
        $app = app();
        $config = $app->make('config');
        $roles = $config['auth']['roles'];

        $result = false;
        $user_role = mb_strtolower($this->role);

        if (isset($roles[$user_role])){
            $result = $roles[$user_role] == $role;
        }

        return $result;
    }

    public function hasRoles($roles){
        $result = false;
        foreach($roles as $role){
            if($this->hasRole($role)){
                $result = true;
                break;
            }
        }

        return $result;
    }
}
