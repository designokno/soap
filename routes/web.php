<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->get('/', ['uses' => 'LoginController@indexAction', 'as' => 'login']);
    $router->post('/', ['uses' => 'LoginController@indexAction']);
    $router->get('/code', ['uses' => 'LoginController@codeAction', 'as' => 'login_code']);
    $router->post('/code', ['uses' => 'LoginController@codeAction']);

});


$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/auth/logout', ['uses' => 'LoginController@logoutAction', 'as' => 'logout']);

    $router->get('/', [
        'uses' => 'TableController@indexAction', 'as' => 'table_index',
        'middleware' => 'role:sizer,manager',
    ]);
    $router->get('/zamer/{zamerId}', [
        'uses' => 'TableController@detailAction', 'as' => 'table_detail',
        'middleware' => 'role:sizer,manager',
    ]);

    $router->post('/saveresultzamer/{zamerId}', [
        'uses' => 'TableController@saveresultzamerAction', 'as' => 'saveresultzamer',
        'middleware' => 'role:sizer',
    ]);
    $router->post('/savedogovor/{zamerId}', [
        'uses' => 'TableController@savedogovorAction', 'as' => 'savedogovor',
        'middleware' => 'role:sizer',
    ]);
    $router->post('/setpay/{zamerId}', [
        'uses' => 'TableController@setpayAction', 'as' => 'setpay',
        'middleware' => 'role:sizer',
    ]);
    $router->post('/setstatusroute/{zamerId}', [
        'uses' => 'TableController@setstatusrouteAction', 'as' => 'setstatusroute',
        'middleware' => 'role:sizer',
    ]);

    $router->get('/files/{zayavNum}', [
        'uses' => 'TableController@filesAction', 'as' => 'table_files',
        'middleware' => 'role:sizer,manager'
    ]);
    $router->get('/download/{fileId}', [
        'uses' => 'TableController@downloadAction', 'as' => 'download_file',
        'middleware' => 'role:sizer,manager'
    ]);
    $router->post('/upload/{zayavNum}', [
        'uses' => 'TableController@uploadAction', 'as' => 'upload_files',
        'middleware' => 'role:sizer,manager'
    ]);
    $router->post('/additem', [
        'uses' => 'TableController@additemAction', 'as' => 'table_additem',
        'middleware' => 'role:manager'
    ]);
    $router->post('/addzamer/{itemnum}', [
        'uses' => 'TableController@addzamerAction', 'as' => 'table_addzamer',
        'middleware' => 'role:manager'
    ]);

    $router->get('/reports', [
        'uses' => 'ReportController@indexAction', 'as' => 'report_index',
        'middleware' => 'role:sizer',
    ]);
    $router->get('/reports/{identifier}', [
        'uses' => 'ReportController@reportparamsAction', 'as' => 'report_params',
        'middleware' => 'role:sizer',
    ]);
    $router->post('/reports/generate', [
        'uses' => 'ReportController@reportgenerateAction', 'as' => 'report_generate',
        'middleware' => 'role:sizer',
    ]);

    $router->get('/search', [
        'uses' => 'SearchController@indexAction', 'as' => 'search_index',
        'middleware' => 'role:sizer',
    ]);
});
