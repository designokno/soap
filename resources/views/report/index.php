<?php


/**
 * @var $this Illuminate\View\Engines\PhpEngine
 */

/**
 * @var $reports array|null
 */


app('viewHelpers')->registerJs('js/report/list.js');

?>

<?php if ($reports): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Наименование отчёта</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($reports as $row): ?>
                        <tr>
                            <td>
                                <a class="launch_modal" data-identifier="<?php echo $row['Идентификатор']?>" href="<?php echo route('report_params', [
                                    'identifier' => $row['Идентификатор']
                                ]) ?>"><?php echo $row['Наименование'] ?></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-danger" role="alert">Отчёты отсутствуют</div>

        </div>
    </div>
<?php endif ?>


<div class="modal fade" id="report_modal" tabindex="-1" role="dialog">
    <form class="modal-dialog modal-report" role="document" method="post" action="<?php echo route('report_generate', [

    ]); ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Формирование отчёта</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Сформировать</button>
            </div>
        </div><!-- /.modal-content -->
    </form><!-- /.modal-dialog -->
</div><!-- /.modal -->