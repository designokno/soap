<?php

/**
 * @var $this Illuminate\View\Engines\PhpEngine
 */

/**
 * @var $detail array
 */



?>


<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#">Детально</a></li>
            <li role="presentation"><a href="<?php echo $link = route('table_files', [
                    'zamerId' => $zamerId,
                    'zayavNum' => $zayavNum,
                    'date' => $date
                ]);?>">Файлы</a></li>
        </ul>
        <?php echo view('components/zamer_buttons', [
            'zamerId' => $zamerId,
            'zayavNum' => $zayavNum,
            'date' => $date
        ]); ?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <tbody>
                <?php foreach ($detail as $fieldName => $fieldValue): ?>
                    <tr>
                        <td class="col-xs-4 text-right">
                            <strong><?php echo $fieldName ?>:</strong>
                        </td>
                        <td class="col-xs-8">
                            <?php echo nl2br($fieldValue); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <?php echo view('components/zamer_status', [
            'zamerId' => $zamerId,
            'zayavNum' => $zayavNum,
            'date' => $date,
            'state'=> $state
        ]); ?>
    </div>
</div>


