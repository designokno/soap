<?php

/**
 * @var $this Illuminate\View\Engines\PhpEngine
 */

/**
 * @var $message string
 */


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Авторизация. Список замеров</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/login.css">

</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">


                </div>
            </div>

            <div class="inner cover">
                <img src="/images/logo.png"/>
                <?php if (isset($message) && !empty($message)): ?>
                    <div class="alert alert-danger" role="alert"><?php echo $message ?></div>
                <?php endif ?>
                <form method="post" action="<?php echo route('login'); ?>" class="form-horizontal">
                    <div class="form-group">
                        <label for="userLogin" class="col-sm-2 control-label">Логин</label>
                        <div class="col-sm-10">
                            <input type="text" id="userLogin" class="form-control" name="login"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="userPassword" class="col-sm-2 control-label">Пароль</label>
                        <div class="col-sm-10">
                            <input type="password" id="userPassword" class="form-control" name="password"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block" type="submit">Вход</button>
                        </div>
                        <!--<div class="col-xs-6">
                            <a href="<?php /*echo route('login_code'); */?>" class="btn btn-danger btn-lg btn-block">У меня
                                есть код</a>
                        </div>-->
                    </div>
                </form>

            </div>

            <div class="mastfoot">
                <div class="inner">

                </div>
            </div>

        </div>

    </div>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>