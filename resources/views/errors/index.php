<?php


/**
 * @var $this Illuminate\View\Engines\PhpEngine
 */

/**
 * @var $exception \Symfony\Component\Debug\Exception\FlattenException
 */


?>

<div class="row">
    <div class="col-xs-12">
        <?php if ($exception->getStatusCode() == 403): ?>
            <h1>Доступ запрещён</h1>
            <p>Ваших прав недостаточно для того, чтобы просмотреть содержимое данной страницы</p>
        <?php elseif ($exception->getStatusCode() == 404): ?>
            <h1>Страница не найдена</h1>
        <?php else: ?>
            <h1>Произошла ошибка</h1>
            <p>Пожалуйста, сообщите администратору</p>
        <?php endif; ?>
    </div>
</div>