<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 15.04.18
 * Time: 18:36
 */


$soap = app('soap');


/*echo $soap->getfunction(['name' => 'savedogovor'])->return;
exit;*/

//var_dump($soap->getrezultzamerlist());


$rezultzamerTypes = $soap->getResultZamerList();
$statuszamerTypes = $soap->getStatusZamerList();


$dtdostavkaDates = $soap->getOpenDate('Доставка');
$dtmontageDates = $soap->getOpenDate('Монтаж');
$typeoplataTypes = $soap->getOplataTypeList();

$dogovorList = $soap->getDogovorList($zayavNum);
$variantOplataList = $soap->getVariantOplataList();

$formPrefix = isset($formPrefix)?$formPrefix:'';

app('viewHelpers')->registerJs('js/modal_buttons.js');


?>

<div class="row">
    <div class="col-xs-12">
<br/>
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <a href="#" data-toggle="modal" data-target="#<?php echo $formPrefix?>saveresultzamerModal" class="btn btn-danger">Записать
                результат</a>
            <a href="#" data-toggle="modal" data-target="#<?php echo $formPrefix?>savedogovorModal" class="btn btn-info">Создать договор</a>
            <a href="#" data-toggle="modal" data-target="#<?php echo $formPrefix?>setpayModal" class="btn btn-warning">Внести оплату</a>
        </div>
        <br/>
    </div>
</div>

<div class="modal fade" id="<?php echo $formPrefix?>saveresultzamerModal" tabindex="-1" role="dialog">
    <form class="modal-dialog modal-saveresultzamer" role="document" method="post" action="<?php echo route('saveresultzamer', [
        'zamerId' => $zamerId,
        'zayavNum' => $zayavNum,
        'date' => $date
    ]); ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Запись результата замера</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="<?php echo $formPrefix?>rezultzamerZamer">Результат выезда</label>
                    <select id="<?php echo $formPrefix?>rezultzamerZamer" name="rezultzamer" class="form-control">
                        <option/>
                        <?php foreach ($rezultzamerTypes as $key => $value): ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="<?php echo $formPrefix?>commentZamer">Комментарий</label>
                    <textarea name="comment" id="<?php echo $formPrefix?>commentZamer" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="<?php echo $formPrefix?>statusZamer">Статус</label>
                    <select id="<?php echo $formPrefix?>statusZamer" name="status" class="form-control">
                        <option/>
                        <?php foreach ($statuszamerTypes as $key => $value): ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Записать</button>
            </div>
        </div><!-- /.modal-content -->
    </form><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="<?php echo $formPrefix?>savedogovorModal" tabindex="-1" role="dialog">
    <form class="modal-dialog modal-savedogovor" role="document" method="post" action="<?php echo route('savedogovor', [
        'zamerId' => $zamerId,
        'zayavNum' => $zayavNum,
        'date' => $date
    ]); ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Создание договора</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="<?php echo $formPrefix?>summadogovorDogovor">Сумма договора</label>
                    <input id="<?php echo $formPrefix?>summadogovorDogovor" type="number" name="summadogovor" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="<?php echo $formPrefix?>dtdostavkaDogovor">Дата доставки</label>
                    <select id="<?php echo $formPrefix?>dtdostavkaDogovor" name="dtdostavka" class="form-control">
                        <option/>
                        <?php foreach ($dtdostavkaDates as $key => $value): ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="<?php echo $formPrefix?>dtmontageDogovor">Дата монтажа</label>
                    <select id="<?php echo $formPrefix?>dtmontageDogovor" name="dtmontage" class="form-control">
                        <option/>
                        <?php foreach ($dtmontageDates as $key => $value): ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="isdemontage"> Есть демонтаж
                    </label>
                </div>

                <div class="form-group">
                    <label for="<?php echo $formPrefix?>summapredoplataDogovor">Сумма предоплаты</label>
                    <input id="<?php echo $formPrefix?>summapredoplataDogovor" type="number" name="summapredoplata" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="<?php echo $formPrefix?>typeoplataDogovor">Тип оплаты</label>
                    <select id="<?php echo $formPrefix?>typeoplataDogovor" name="typeoplata" class="form-control">
                        <option/>
                        <?php foreach ($typeoplataTypes as $key => $value): ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="<?php echo $formPrefix?>commentDogovor">Комментарий замерщика</label>
                    <textarea name="comment" id="<?php echo $formPrefix?>commentDogovor" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <label for="<?php echo $formPrefix?>commentmontageDogovor">Комментарий монтажному отделу</label>
                    <textarea name="commentmontage" id="<?php echo $formPrefix?>commentmontageDogovor" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <label for="<?php echo $formPrefix?>commentdostavkaDogovor">Комментарий логистике</label>
                    <textarea name="commentdostavka" id="<?php echo $formPrefix?>commentdostavkaDogovor" class="form-control"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Создать</button>
            </div>
        </div><!-- /.modal-content -->
    </form><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="<?php echo $formPrefix?>setpayModal" tabindex="-1" role="dialog">
    <form class="modal-dialog modal-setpay" role="document" method="post" action="<?php echo route('setpay', [
        'zamerId' => $zamerId,
        'zayavNum' => $zayavNum,
        'date' => $date
    ]); ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Внесение оплаты</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <label for="<?php echo $formPrefix?>dogovorPayment">Номер договора</label>
                    <select id="<?php echo $formPrefix?>dogovorPayment" name="dogovor" class="form-control">
                        <option/>
                        <?php foreach ($dogovorList as $key => $value): ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="<?php echo $formPrefix?>summPayment">Сумма</label>
                    <input id="<?php echo $formPrefix?>summPayment" type="number" name="summ" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="<?php echo $formPrefix?>typeoplataPayment">Способ оплаты</label>
                    <select id="<?php echo $formPrefix?>typeoplataPayment" name="typeoplata" class="form-control">
                        <option/>
                        <?php foreach ($variantOplataList as $key => $value): ?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Записать</button>
            </div>
        </div><!-- /.modal-content -->
    </form><!-- /.modal-dialog -->
</div><!-- /.modal -->
