<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 15.04.18
 * Time: 18:36
 */


/**
 * @var $title string
 * @var $funcname string
 * @var $fieldname string
 * @var $required bool
 */
$code = <<<EOF
$(function () {
    $('#datetimepicker-{$funcname}-{$fieldname}-wrapper').datetimepicker({
            locale: 'ru',
            format: 'YYYY.MM.DD'
        });
    });
EOF;
app('viewHelpers')->registerJsCode($code);
?>

<div class="form-group">
    <label for="datetimepicker-<?php echo "{$funcname}-{$fieldname}" ?>"><?php echo $title; ?></label>

    <div class='input-group date' id='datetimepicker-<?php echo "{$funcname}-{$fieldname}" ?>-wrapper'>
        <input type="text" name="<?php echo $fieldname ?>" id="datetimepicker-<?php echo "{$funcname}-{$fieldname}" ?>"
               class="form-control" <?php echo $required ? 'required' : '' ?>/>

        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
    </div>


</div>
