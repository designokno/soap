<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 15.04.18
 * Time: 18:36
 */


/**
 * @var $title string
 * @var $funcname string
 * @var $fieldname string
 * @var $required bool
 * @var $items []
 */

?>

<div class="form-group">
    <label for="<?php echo "{$funcname}-{$fieldname}" ?>"><?php echo $title; ?></label>
    <select name="<?php echo $fieldname ?>" id="<?php echo "{$funcname}-{$fieldname}" ?>"
            class="form-control" <?php echo $required ? 'required' : '' ?>>
        <?php foreach ($items as $key => $value): ?>
            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
        <?php endforeach; ?>
    </select>
</div>
