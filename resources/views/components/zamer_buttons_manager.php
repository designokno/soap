<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 15.04.18
 * Time: 18:36
 */


$soap = app('soap');


/*echo $soap->getfunction(['name' => 'savedogovor'])->return;
exit;*/

//var_dump($soap->getrezultzamerlist());


$rezultzamerTypes = $soap->getResultZamerList();
$statuszamerTypes = $soap->getStatusZamerList();


//$dtdostavkaDates = $soap->getOpenDate('Доставка');
//$dtmontageDates = $soap->getOpenDate('Монтаж');
//$typeoplataTypes = $soap->getOplataTypeList();

//$dogovorList = $soap->getDogovorList($zayavNum);
//$variantOplataList = $soap->getVariantOplataList();

$formPrefix = isset($formPrefix) ? $formPrefix : '';

app('viewHelpers')->registerJs('js/modal_buttons_manager.js');


?>

<div class="row">
    <div class="col-xs-12">
        <br/>
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <a href="#" data-toggle="modal" data-target="#<?php echo $formPrefix ?>addzamerModal"
               class="btn btn-danger">Запись на замер</a>
            <a href="#" data-toggle="modal" data-target="#<?php echo $formPrefix ?>transferitemModal"
               class="btn btn-warning">Перезвонить/в офис</a>
            <a href="#" data-toggle="modal" data-target="#<?php echo $formPrefix ?>addclaimModal" class="btn btn-info">Претензия</a>
        </div>
        <br/>
    </div>
</div>

<div class="modal fade" id="<?php echo $formPrefix ?>addzamerModal" tabindex="-1" role="dialog">
    <form class="modal-dialog modal-addzamer" role="document" method="post" action="<?php echo route('table_addzamer', [
        'itemnum' => $itemnum,
    ]); ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Добавление замера</h4>
            </div>
            <div class="modal-body">
                <?php echo $soap->renderFormField('Дата', 'addzamer', 'dt', ['template'=>'components/form/fields/date']); ?>
                <?php echo $soap->renderFormField('Время', 'addzamer', 'time', ['template'=>'components/form/fields/time']); ?>
                <?php echo $soap->renderFormField('Информация', 'addzamer', 'info'); ?>
                <?php echo $soap->renderFormField('Комментарий', 'addzamer', 'comment', []); ?>
                <?php echo $soap->renderFormField('Статус обращения', 'addzamer', 'status'); ?>
                <?php echo $soap->renderFormField('Цель замера', 'addzamer', 'object'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
        </div><!-- /.modal-content -->
    </form><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="<?php echo $formPrefix ?>transferitemModal" tabindex="-1" role="dialog">
    <form class="modal-dialog modal-transferitem" role="document" method="post"
          action="<?php echo route('savedogovor', [
              'zamerId' => $itemnum
          ]); ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Создание договора</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Создать</button>
            </div>
        </div><!-- /.modal-content -->
    </form><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="<?php echo $formPrefix ?>addclaimModal" tabindex="-1" role="dialog">
    <form class="modal-dialog modal-addclaim" role="document" method="post" action="<?php echo route('setpay', [
        'zamerId' => $itemnum
    ]); ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Внесение оплаты</h4>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary">Записать</button>
            </div>
        </div><!-- /.modal-content -->
    </form><!-- /.modal-dialog -->
</div><!-- /.modal -->
