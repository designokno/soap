<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 15.04.18
 * Time: 18:36
 */


$soap = app('soap');

$formPrefix = isset($formPrefix) ? $formPrefix : '';

app('viewHelpers')->registerJs('js/zamer_status.js');


$link = route('setstatusroute', [
    'zamerId' => $zamerId,
    'zayavNum' => $zayavNum,
    'date' => $date
]);

$is_disabled_accepted = in_array($state, [1, 2]) ? ' disabled' : '';
$is_disabled_arrived = in_array($state, [2]) ? ' disabled' : '';

?>

<div class="row">
    <div class="col-xs-12">
        <br/>
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <a href="<?php echo $link ?>" data-zamernum="<?php echo $zayavNum?>" class="btn btn-danger zamer-accepted <?php echo $is_disabled_accepted;?>">Замер принял</a>
            <a href="<?php echo $link ?>" data-zamernum="<?php echo $zayavNum?>" class="btn btn-info zamer-arrived <?php echo $is_disabled_arrived;?>">Прибыл на замер</a>
        </div>
        <br/>
    </div>
</div>