<?php
use \App\Http\Controllers\TableController;

/**
 * @var $this Illuminate\View\Engines\PhpEngine
 */

/**
 * @var $table_rows array|null
 * @var $table_keys array|null
 * @var $text string
 */


?>

<?php if ($table_rows): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-success" role="alert">Вы искали <strong><?php echo htmlspecialchars($text); ?></strong></div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <?php foreach ($table_keys as $key): ?>
                            <th><?php echo $key ?></th>
                        <?php endforeach; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($table_rows as $row): ?>
                        <?php
                        $zamerId = $row[TableController::ZAMER_KEY];
                        list(,$zayavNum) = explode('-', $row[TableController::ZAYAVKA_KEY]);
                        ?>
                        <tr>
                            <?php foreach ($table_keys as $i => $key): ?>
                                <td>
                                    <?php

                                    $value = isset($row[$key]) ? $row[$key] : null;
                                    if ($i == 0) {
                                        $link = route('table_detail', [
                                            'zamerId' => $zamerId,
                                            'zayavNum' => $zayavNum
                                        ]);
                                        echo <<<EOF
<a href="{$link}">{$value}</a>
EOF;
                                    }
                                    else {
                                        echo $value;
                                    }
                                    ?>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-danger" role="alert">По запросу <strong><?php echo htmlspecialchars($text); ?></strong> результаты отсутствуют</div>
        </div>
    </div>
<?php endif ?>