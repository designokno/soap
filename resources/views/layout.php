<?php

/**
 * @var $this Illuminate\View\Engines\PhpEngine
 */

/**
 * @var $table_rows array|null
 * @var $table_keys array|null
 */

$soap = app('soap');

$user = app()->request->user();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Список замеров</title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
          crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <link rel="stylesheet" href="/css/custom.css">


    <?php echo app('viewHelpers')->renderCss(); ?>
</head>

<body>

<!-- Fixed navbar -->

<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style='padding:0 15px;margin-top: -16px;' href="/"><img height="80"
                                                                                            src="/images/logo.png"/></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <?php if ($user): ?>
                    <?php
                    $link_index_report_params = [];
                    $link_index_params = [];
                    if (isset($_GET['date'])) {
                        $link_index_params['date'] = $_GET['date'];
                    }
                    $link_index = route('table_index', $link_index_params);
                    $link_index_report = route('report_index', $link_index_report_params);
                    ?>
                    <?php if ($user->hasRole('sizer')): ?>
                        <li><a href="<?php echo $link_index_report ?>">Отчёты</a></li>
                        <li><a href="<?php echo $link_index ?>">Мои замеры</a></li>
                    <?php endif ?>
                    <?php if ($user->hasRole('manager')): ?>
                        <li><a href="#" data-toggle="modal" data-target="#create-request">Создать заявку</a></li>
                        <li><a href="<?php echo $link_index ?>">Мои заявки</a></li>
                    <?php endif ?>

                    <li><a href="<?php echo route('logout') ?>">Выход (<?php echo $user->username ?>)</a></li>
                <?php else: ?>
                    <li><a href="<?php echo route('login') ?>">Войти</a></li>
                <?php endif; ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<?php if ($user): ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <form method="get" action="<?php echo route('search_index') ?>">
                    <div class="input-group">
                        <?php
                        $text = htmlspecialchars(app('request')->input('text'));
                        ?>
                        <input type="text" name="text" value="<?php echo $text ?>" required class="form-control"
                               placeholder="Я ищу...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Найти!</button>
                    </span>
                    </div><!-- /input-group -->
                    <br/>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>

<!-- Begin page content -->
<div class="container">
    <?php echo $content; ?>
</div>

<?php if ($user->hasRole('manager')): ?>
    <?php app('viewHelpers')->registerJs('js/modal_create_request.js'); ?>
    <div class="modal fade" id="create-request" tabindex="-1" role="dialog" aria-labelledby="create-request-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="create-request-label">Создание заявки</h4>
                </div>
                <form method="post" action="<?php echo route('table_additem') ?>">
                    <div class="modal-body">
                        <?php echo $soap->renderFormField('Источник обращения', 'additem', 'sourceitem'); ?>
                        <?php echo $soap->renderFormField('Наименование', 'additem', 'name'); ?>
                        <?php echo $soap->renderFormField('Номер телефон', 'additem', 'phone1'); ?>
                        <?php echo $soap->renderFormField('Дополнительный номер телефона', 'additem', 'phone2'); ?>
                        <?php echo $soap->renderFormField('Адрес', 'additem', 'address'); ?>
                        <?php echo $soap->renderFormField('Email', 'additem', 'email', ['template' => 'components/form/fields/email']); ?>
                        <?php echo $soap->renderFormField('Источник рекламы', 'additem', 'inforeklama'); ?>
                        <?php echo $soap->renderFormField('Комментарий', 'additem', 'comment', ['template' => 'components/form/fields/textarea']); ?>
                        <?php echo $soap->renderFormField('Результат', 'additem', 'result'); ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary">Создать</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif ?>

<!-- Bootstrap core JavaScript
================================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/locale/ru.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>


<script type="text/javascript">
    $(function () {
        $('#datetimepicker').datetimepicker({
            locale: 'ru',
            format: 'YYYY.MM.DD'
        });
        $('#datetimepicker').on("dp.change", function (e) {
//            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            document.location.href = "<?php echo route('table_index');?>?date=" + $('#datetimepicker-field').val();
        });
    });
</script>

<?php echo app('viewHelpers')->renderJs(); ?>

</body>
</html>
