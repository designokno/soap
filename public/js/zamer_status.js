$(document).ready(function () {
    $('.zamer-accepted, .zamer-arrived').on('click', function(e){
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {

            var status = '';
            if ($(this).hasClass('zamer-accepted')) {
                status = 'Замер принял';
            } else if ($(this).hasClass('zamer-arrived')) {
                status = 'Прибыл на замер';
            }

            $.post($(this).attr('href'), {
                'status': status
            }).done(function (data) {
                alert("Статус замера изменён");
            });
        }
    });
});