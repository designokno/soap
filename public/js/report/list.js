$(document).ready(function () {

    $('.launch_modal').on("click", function (e) {
        e.preventDefault();
        var modal = $("#report_modal");
        var modal_body = $(".modal-body", modal);

        modal_body.html("");

        var link = $(this).attr("href");
        var identifier = $(this).data('identifier');


        $("<input/>", {
            "type": "hidden",
            "name": "identifier",
            "value": identifier
        }).appendTo(modal_body);

        $.getJSON(link).done(function (data) {
            var fields = $("<div/>");
            $.each(data.params, function () {
                var type = this.type;
                var title = this.title;
                var fieldDiv = null;

                if (type === "bool") {
                    fieldDiv = $("<div class='checkbox'/>").appendTo(modal_body);
                }
                else {
                    fieldDiv = $("<div class='form-group'/>").appendTo(modal_body);
                }

                var label = $('<label/>', {
                    "text": title
                }).appendTo(fieldDiv);


                if (type === "string") {
                    $('<input/>', {
                        "type": "text",
                        "class": "form-control",
                        "required": "required",
                        "name": "param[" + title + "]"
                    }).appendTo(fieldDiv);
                } else if (type === "int") {
                    $('<input/>', {
                        "type": "number",
                        "class": "form-control",
                        "required": "required",
                        "name": "param[" + title + "]"
                    }).appendTo(fieldDiv);
                } else if (type === "date") {

                    var dateWrapper = $("<div class='input-group date'/>").appendTo(fieldDiv);

                    var field = $('<input/>', {
                        "type": "text",
                        "class": "form-control",
                        "required": "required",
                        "name": "param[" + title + "]"
                    }).appendTo(dateWrapper);


                    $("<span/>", {
                        "class": "input-group-addon",
                        "html": "<i class='glyphicon glyphicon-th'></i>"
                    }).appendTo(dateWrapper);


                    $(field).datetimepicker({
                        locale: 'ru',
                        format: 'YYYY.MM.DD'
                    });
                } else if (type === "bool") {

                    $('<input/>', {
                        "type": "checkbox",
                        "name": "param[" + title + "]",
                        "value": "Да"
                    }).prependTo(label);
                    $('<input/>', {
                        "type": "hidden",
                        "name": "param[" + title + "]",
                        "value": "Нет"
                    }).prependTo(label);
                } else if (type === "list") {
                    var select = $("<select/>", {
                        "class": "form-control",
                        "required": "required",
                        "name": "param[" + title + "]"
                    }).appendTo(fieldDiv);

                    $.each(this.values, function () {
                        $("<option/>", {
                            "value": this,
                            "text": this
                        }).appendTo(select);
                    });
                }

                modal.modal("show");
            });
        });
    });

});