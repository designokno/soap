$(document).ready(function () {
    var clearForm = function (form) {
        $('input[type="text"], textarea, input[type="hidden"]', $(form)).not('[name="csrfmiddlewaretoken"]').val('');
        $('input:checked', $(form)).not(':disabled').prop('checked', false);
    };

    $('.modal-savedogovor').on('submit', function (e) {
        e.preventDefault();
        var form = this;
        var modal = $(form).parent();

        $.post($(this).attr('action'), $(this).serialize()).done(function (data) {
            if(data.result){
                form.reset();
                $(modal).modal('hide');
                alert("Договор успешно создан");
            }else{
                alert(data.message);
            }
        });
    });
    $('.modal-saveresultzamer').on('submit', function (e) {
        e.preventDefault();
        var form = this;
        var modal = $(form).parent();

        $.post($(this).attr('action'), $(this).serialize()).done(function (data) {
            if(data.result){
                form.reset();
                $(modal).modal('hide');
                alert("Результат замера записан");
            }else{
                alert(data.message);
            }
        });
    });
    $('.modal-setpay').on('submit', function (e) {
        e.preventDefault();
        var form = this;
        var modal = $(form).parent();

        $.post($(this).attr('action'), $(this).serialize()).done(function (data) {
            if(data.result){
                form.reset();
                modal.modal('hide');
                alert("Результат оплаты записан");
            }else{
                alert(data.message);
            }
        });
    });
});