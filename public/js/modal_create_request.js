$(document).ready(function () {
    var clearForm = function (form) {
        $('input[type="text"], textarea, input[type="hidden"]', $(form)).not('[name="csrfmiddlewaretoken"]').val('');
        $('input:checked', $(form)).not(':disabled').prop('checked', false);
    };

    $('#create-request form').on('submit', function (e) {
        e.preventDefault();
        var form = this;
        var modal = $(form).parent();

        $.post($(this).attr('action'), $(this).serialize()).done(function (data) {
            if(data.result || data.message === 'Обращение создано'){
                form.reset();
                $(modal).modal('hide');

                alert("Заявка успешно создана");
                if(window['MANAGER_ITEM_LIST']){
                    document.location.reload();
                }
            }else{
                alert(data.message);
            }
        });
    });
});