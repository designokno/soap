$(document).ready(function () {
    var clearForm = function (form) {
        $('input[type="text"], textarea, input[type="hidden"]', $(form)).not('[name="csrfmiddlewaretoken"]').val('');
        $('input:checked', $(form)).not(':disabled').prop('checked', false);
    };

    $('.modal-addzamer form').on('submit', function (e) {
        e.preventDefault();
        var form = this;
        var modal = $(form).parent();

        $.post($(this).attr('action'), $(this).serialize()).done(function (data) {
            if(data.result){
                form.reset();
                $(modal).modal('hide');
                alert("Замер успешно добавлен");
            }else{
                alert(data.message);
            }
        });
    });
});